Responses
=========

An API endpoint returns responses, which consist of a HTTP status code and an optional application/json-encoded body.

Errors
------

If an error while performing the request occurs, the API will return a HTTP status code equal or greater than 400. Then, the API will return also a JSON object in the body with the parameter error, which describes the error. This is compatible to OAuth2, especially because OAuth2 errors are handled by the same functionality. In addition to the error field, the object can contain other error-specific fields, which describe the errors more specific.

::

  {
    "error": {string},
    ...
  }

See :doc:`Errors </api/errors>` for a full list of API errors.

