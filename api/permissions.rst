Permissions
===========

Pixelmerge maintains several permissions for its API endpoints. These permissions are also OAuth2 scopes, that means, you have to obtain them on Authorization Grant

+---------------+--------------------------------------------------------------+
| Name          | Description                                                  |
+===============+==============================================================+
| get_private\  | Access to objects which hold a scope which is private        |
| _scope        |                                                              |
+---------------+--------------------------------------------------------------+
| create_post   | Create new objects which are related to posts                |
+---------------+--------------------------------------------------------------+
| get_services  | Read service objects                                         |
+---------------+--------------------------------------------------------------+
| add_service   | Create service objects                                       |
+---------------+--------------------------------------------------------------+
| delete_servi\ | Delete service objects                                       |
| ce            |                                                              |
+---------------+--------------------------------------------------------------+
| get_views     | Read view objects                                            |
+---------------+--------------------------------------------------------------+
| create_view   | Create view objects                                          |
+---------------+--------------------------------------------------------------+
| delete_view   | Delete view objects                                          |
+---------------+--------------------------------------------------------------+
| get_settings  | Read settings                                                |
+---------------+--------------------------------------------------------------+
| set_settings  | Set settings                                                 |
+---------------+--------------------------------------------------------------+

