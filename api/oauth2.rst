OAuth2
======

Pixelmerge uses OAuth2 to authorize API Clients for specific Endpoints. We implement the latest OAuth2 Draft (at the moment 31).

Authorization Code Grant
------------------------

Pixelmerge supports the Authorization Code Grant as preferred grant:

1. Redirect the user to https://pixelmerge.me/oauth2/connect with the following parameters as query components encoded as application/x-www-urlencoded [#W3C.REC-html401-19991224]_

   response_type
       Expected response, must be "code" for Authorization Code Grant
   client_id
       Your applications Client ID
   redirect_uri
       A URL to redirect after permission grant
   scope
       List of requested :doc:`permissions </api/permissions>`. Must be a space-separated list of permission tokens obtained from /api/info. If omitted, the request will implicit contain the standard scopes of your Client.
   state
       An optional state to determine the request in your callback. We (and the draft) recommend, that you are using a state

2. The user will see a dialog, which asks him to permit or deny the request. If the user permits the authorization grant, then the user will be redirected to redirect_url with the following additional parameters in the Query component, which is encoded as application/x-www-urlencoded [#W3C.REC-html401-19991224]_:

   code
       The authorization code to obtain an access token
   state
       If you supplied a state in step 1, then you will get it here

3. Call https://pixelmerge.me/oauth2/access_token on server side with the following parameters as query component encoded as application/x-www-urlencoded [#W3C.REC-html401-19991224]_:

   grant_type
       The OAuth2 grant type, in this case "authorization_code"
   code
       The obtained authorization code from step 2
   redirect_uri
       The redirect_uri parameter from step 1
   client_id
       Your Client ID
   client_secret
       Your Client secret

4. Step 3 will respond with an body encoded as application/json [#RFC4627]_

.. rubric:: Footnotes

.. [#W3C.REC-html401-19991224] `Hors, A., Raggett, D., and I. Jacobs, "HTML 4.01 Specification", World Wide Web Consortium Recommendation REC-html401-19991224, December 1999. <http://www.w3.org/TR/1999/REC-html401-19991224>`_
.. [#RFC4627] `Crockford, D., "The application/json Media Type for JavaScript Object Notation (JSON)", RFC 4627, July 2006. <http://tools.ietf.org/html/rfc4627>`_
