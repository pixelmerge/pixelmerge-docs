Location
========

GET /locations/private
----------------------

Method
  GET
Path
  /locations/private
Parameters
  This endpoint accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  get_private_scope
Returns
  :doc:`Array of location objects </api/objects/locations>`

GET /locations/public
---------------------

Method
  GET
Path
  /locations/public
Parameters
  This endpoint accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  None
Returns
  :doc:`Array of location objects </api/objects/locations>`

GET /locations/public/<username>
--------------------------------

Method
  GET
Path
  /locations/public/<username>
Parameters
  +----------+----------+------------------------------------------------------+
  | Name     | Type     | Description                                          |
  +==========+==========+======================================================+
  | username | string   | Name of a registered user                            |
  +----------+----------+------------------------------------------------------+
  | count    | number   | Number of records to receive as query parameter      |
  +----------+----------+------------------------------------------------------+
  | since_t\ | number   | Time of the earliest object to return (query)        |
  | ime      |          |                                                      |
  +----------+----------+------------------------------------------------------+

  In addition to this, this endpoint accepts all generic collection parameters.
Authorization
  None
Permissions
  None
Returns
  :doc:`Array of location objects </api/objects/locations>`

GET /locations/id/<id>
----------------------

Method
  GET
Path
  /locations/id/<id>
Parameters
  +------------+---------+-----------------------------------------------------+
  | Name       | Type    | Description                                         |
  +============+=========+=====================================================+
  | id         | string  | ID of a location object                             |
  +------------+---------+-----------------------------------------------------+
Authorization
  None if the scope of the object is "public", otherwise :doc:`OAuth2 </api/oauth2>`
Permissions
  If the scope of the object is "private":

  * get_private_scope
Returns
  :doc:`Location object </api/objects/locations>`

