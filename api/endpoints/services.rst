Service
=======

GET /services
-------------

Method
  GET
Path
  /services
Parameters
  None
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  get_services
Returns
  :doc:`Array of service objects </api/objects/services>`

GET /services/<id>
------------------

Method
  GET
Path
  /services/<id>
Parameters
  +------------+----------+----------------------------------------------------+
  | Name       | Type     | Description                                        |
  +============+==========+====================================================+
  | id         | string   | ID of a service object                             |
  +------------+----------+----------------------------------------------------+
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  get_services
Returns
  :doc:`Array of service objects </api/objects/services>`

DELETE /services/<id>
---------------------

Method
  DELETE
Path
  /services/<id>
Parameters
  +------------+----------+----------------------------------------------------+
  | Name       | Type     | Description                                        |
  +============+==========+====================================================+
  | id         | string   | ID of a service object                             |
  +------------+----------+----------------------------------------------------+
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  delete_service
Returns
  Nothing

POST /services
--------------

