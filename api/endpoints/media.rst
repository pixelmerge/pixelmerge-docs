Medium
======

GET /media/private
------------------

Method
  GET
Path
  /media/private
Parameters
  This endpoint accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  get_private_scope
Returns
  :doc:`Array of medium objects </api/objects/media>`

GET /media/public
-----------------

Method
  GET
Path
  /media/public
Parameters
  This endpoints accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  None
Returns
  :doc:`Array of medium objects </api/objects/media>`

GET /media/public/<username>
----------------------------

Method
  GET
Path
  /media/public/<username>
Parameters
  +------------+-----------+---------------------------------------------------+
  | Name       | Type      | Description                                       |
  +============+===========+===================================================+
  | username   | string    | Name of a registered user                         |
  +------------+-----------+---------------------------------------------------+

  In addition to this, this endpoint accepts all generic collection parameters
Authorization
  None
Permissions
  None
Returns
  :doc:`Array of medium objects </api/objects/media>`

GET /media/id/<id>
------------------

Method
  GET
Path
  /media/id/<id>
Parameters
  +----------+------------+----------------------------------------------------+
  | Name     | Type       | Description                                        |
  +==========+============+====================================================+
  | id       | string     | ID of a medium object                              |
  +----------+------------+----------------------------------------------------+
Authorization
  None if the scope of the object is "public", otherwise :doc:`OAuth2 </api/oauth2>`
Permissions
  If the scope of the object is not "private":

  * get_private_scope
Returns
  :doc:`Medium object </api/objects/media>`
