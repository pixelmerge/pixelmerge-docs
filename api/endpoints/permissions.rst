Permissions
===========

GET /permissions
----------------

Method
  GET
Path
  /permissions
Parameters
  None
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  None
Returns
  Array of :doc:`Permissions </api/permissions>`

  ::

    [
      {string}
    ]

DELETE /permissions/<permission>
--------------------------------

Method
  DELETE
Path
  /permissions/<permission>
Parameters
  +-------------+-------------+------------------------------------------------+
  | Name        | Type        | Description                                    |
  +=============+=============+================================================+
  | permission  | string      | :doc:`Permission </api/permissions>`, which    |
  |             |             | should removed from the current access token   |
  +-------------+-------------+------------------------------------------------+
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  None
Returns
  Nothing

