View
====

GET /views
----------

POST /views
-----------

GET /views/<id>
---------------

POST /views/<id>
----------------

DELETE /views/<id>
------------------

