Service groups
==============

GET /services/groups
--------------------

POST /services/groups
---------------------

GET /services/groups/<id>
-------------------------

DELETE /services/groups/<id>
----------------------------

POST /services/groups/<id>/<service_id>
---------------------------------------

DELETE /services/groups/<id>/<service_id>
-----------------------------------------

