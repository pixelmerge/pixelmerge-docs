Post
====

GET /posts/private
------------------

This endpoint returns all private posts

Method
  GET
Path
  /posts/private
Parameters
  This endpoint accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  get_private_scope
Returns
  :doc:`Array of post objects </api/objects/posts>`

GET /posts/public
-----------------

Method
  GET
Path
  /posts/public
Parameters
  This endpoint accepts all generic collection parameters
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  None
Returns
  :doc:`Array of post objects </api/objects/posts>`

GET /posts/public/<username>
----------------------------

Method
  GET
Path
  /posts/public/<username>
Parameters
  +----------+------------+----------------------------------------------------+
  | Name     | Type       | Description                                        |
  +==========+============+====================================================+
  | username | string     | Name of a registered user                          |
  +----------+------------+----------------------------------------------------+

  In addition to this, this endpoint accepts all generic collection parameters
Authorization
  None
Permissions
  None
Returns
  :doc:`Array of post objects </api/objects/posts>`

GET /posts/id/<id>
------------------

Method
  GET
Path
  /posts/id/<id>
Parameters
  +----------+------------+----------------------------------------------------+
  | Name     | Type       | Description                                        |
  +==========+============+====================================================+
  | id       | string     | ID of a post object                                |
  +----------+------------+----------------------------------------------------+
Authorization
  None if the scope of the object is "public", otherwise :doc:`OAuth2 </api/oauth2>`
Permissions
  If the scope of the object ist "private":

  * get_private_scope
Returns
  :doc:`Post object </api/objects/posts>`

POST /posts
-----------

Method
  POST
Path
  /posts
Parameters
  This endpoint accepts a JSON encoded object with the following parameters in the
  request body:

  ::
    
    {
      "text": {string},
      "groups": [
        {string}
      ]
    }

  +---------------+---------+--------------------------------------------------+
  | Name          | Type    | Description                                      |
  +===============+=========+==================================================+
  | text          | string  | Text for the post                                |
  +---------------+---------+--------------------------------------------------+
  | groups        | array   | List of service groups IDs, on which members the |
  |               |         | text will be posted.                             |
  +---------------+---------+--------------------------------------------------+
Authorization
  :doc:`OAuth2 </api/oauth2>`
Permissions
  * create_post
Returns
  * 202 if the post is acceptable
  * 400 if the request is invalid
  * 401 if the user hasn't the permissions
