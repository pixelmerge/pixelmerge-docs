User
====

GET /users/<username>
---------------------

Method
  GET
Path
  /users/<username>
Parameters
  +---------------+----------+-------------------------------------------------+
  | Name          | Type     | Description                                     |
  +===============+==========+=================================================+
  | username      | string   | Name of the user                                |
  +---------------+----------+-------------------------------------------------+
Authorization
  None
Permissions
  None
Returns
  :doc:`User object </api/objects/users>`

