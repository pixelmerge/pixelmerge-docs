Requests
========

The Pixelmerge API receives requests only by HTTPS. A request is sent to an endpoint, which replies with data to the request. A request takes a method, a path, an optional query with parameters, and an optional application/json-encoded body. Authorization is performed by OAuth 2, access tokens must be supplied as a query parameter called "access_token".

At the moment, the requests are not rate-limited, so you can request until we block your IP address or access token manually. This could change in near time, so you should always accept errors.

Generic Collection Parameters
-----------------------------

Some API endpoints, which return a collection, accept generic collection parameters, to subset the responses. In general, any collection, which returns objects with a time, is supported.

+---------------+----------+---------------------------------------------------+
| Name          | Type     | Description                                       |
+===============+==========+===================================================+
| count         | number   | Maximum amount of the objects returned            |
+---------------+----------+---------------------------------------------------+
| since_time    | number   | Instruct, that the API should not return objects  |
|               |          | with a time, which is before the time specified   |
|               |          | in this parameter as UTC Unix timestamp.          |
+---------------+----------+---------------------------------------------------+

All of these parameters must be supplied as query parameters.
