Service Objects
===============

Service objects represent external services from which Pixelmerge imports posts.

::

  {
    "id": {string},

    "username": {string},
    "service": {string},
    "state": {string},
    "realname": {string}
  }

+----------+---------+---------------------------------------------------------+
| Field    | Type    | Description                                             |
+==========+=========+=========================================================+
| id       | string  | An unique ID which specifies a service object           |
+----------+---------+---------------------------------------------------------+
| username | string  | Name which references the user object of the owner of   |
|          |         | the service                                             |
+----------+---------+---------------------------------------------------------+
| service  | string  | Name of the external service (e.g. twitter)             |
+----------+---------+---------------------------------------------------------+
| state    | string  | State of the service. Valid states are "pending",       |
|          |         | "inactive", "active"                                    |
+----------+---------+---------------------------------------------------------+
| realname | string  | Display name of the service account                     |
+----------+---------+---------------------------------------------------------+
