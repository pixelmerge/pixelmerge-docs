Post Objects
============

Post objects represent raw posts, which are not merged and imported direct from services. They contain medium and location objects.

::

  {
    "id": {string},
    "external_url": {string},
    "url": {string},
    "scope": {string},
    "username": {string},
    "service": {string},

    "text": {string},
    "location": {
      "longitude": {number},
      "latitude": {number}
    },

    "media": [
      {
        "type": {string},
        "url": {string}
      }
    ],

    "time": {number},

    "creator": {
      "username": {string},
      "display_name": {string},
      "url": {string},
      "avatar": {string}
    }
  }

+-----------+-------------+---------------------------------------------------+
| Field     | Type        | Description                                       |
+===========+=============+===================================================+
| id        | string      | An unique ID which specifies a post object        |
+-----------+-------------+---------------------------------------------------+
| external\ | string      | A URL which references the specific object on     |
| _url      |             | service side.                                     |
+-----------+-------------+---------------------------------------------------+
| url       | string      | A URL which references the specific object on     |
|           |             | Pixelmerges side                                  |
+-----------+-------------+---------------------------------------------------+
| scope     | string      | Scope of the post object                          |
+-----------+-------------+---------------------------------------------------+
| username  | string      | Name which references the user object of the      |
|           |             | service owner                                     |
+-----------+-------------+---------------------------------------------------+
| text      | string      | The text part of the post as unformatted plain    |
|           |             | text                                              |
+-----------+-------------+---------------------------------------------------+
| location  | object      | Specifies the location of the object; null, if    |
|           |             | the post hasn't any location data.                |
+-----------+-------------+---------------------------------------------------+
| location\ | number      | Longitude of the location                         |
| [longitu\ |             |                                                   |
| de]       |             |                                                   |
+-----------+-------------+---------------------------------------------------+
| location\ | number      | Latitude of the location                          |
| [latitud\ |             |                                                   |
| e]        |             |                                                   |
+-----------+-------------+---------------------------------------------------+
| media     | array       | List of media supplied by the post                |
+-----------+-------------+---------------------------------------------------+
| media[]   | object      | Object which holds the media data                 |
+-----------+-------------+---------------------------------------------------+
| media[][\ | string      | Type of the media, at the moment the following    |
| type]     |             | types are supported:                              |
|           |             |                                                   |
|           |             | * image                                           |
+-----------+-------------+---------------------------------------------------+
| media[][\ | string      | URL which references the source of the medium     |
| url]      |             |                                                   |
+-----------+-------------+---------------------------------------------------+
| time      | number      | Unix Timestamp (UTC) which specifies the creation |
|           |             | datetime of the post                              |
+-----------+-------------+---------------------------------------------------+
| creator   | object      | Object which holds information of the creator on  |
|           |             | service side                                      |
+-----------+-------------+---------------------------------------------------+
| creator[\ | string      | Username of the creator of the post on service    |
| username\ |             | side                                              |
| ]         |             |                                                   |
+-----------+-------------+---------------------------------------------------+
| creator[\ | string      | Display name of the creator of the post on service|
| display\  |             | side                                              |
| _name]    |             |                                                   |
+-----------+-------------+---------------------------------------------------+
| creator[\ | string      | Link to the resource of the creator on service    |
| url]      |             | side                                              |
+-----------+-------------+---------------------------------------------------+
| creator[\ | string      | Link to the user image on service side (Avatar)   |
| avatar]   |             |                                                   |
+-----------+-------------+---------------------------------------------------+
