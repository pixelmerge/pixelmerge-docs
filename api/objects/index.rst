Objects
=======

The Pixelmerge API responds with API objects, which are specified in this section:

.. toctree::
   
   posts
   users
   locations
   media
   services
   views
