Medium Objects
==============

Medium objects represent any type of mediums, e.g. images, videos, documents.

::

  {
    "id": {string},

    "username": {string},
    "service": {string},
    "time": {number},
    "url": {string},
    "scope": {string},
    "post": {string},
    "type": {string}
  }

+---------------+----------+---------------------------------------------------+
| Field         | Type     | Description                                       |
+===============+==========+===================================================+
| id            | string   | An unique ID which specifies a media object       |
+---------------+----------+---------------------------------------------------+
| username      | string   | Name which references a user which owns the post  |
+---------------+----------+---------------------------------------------------+
| service       | string   | An ID which references the service object         |
+---------------+----------+---------------------------------------------------+
| time          | number   | Time of the creation of the referenced post       |
|               |          | object                                            |
+---------------+----------+---------------------------------------------------+
| url           | string   | URL which references the source of the medium     |
+---------------+----------+---------------------------------------------------+
| scope         | string   | Scope of the medium                               |
+---------------+----------+---------------------------------------------------+
| post          | string   | ID which references the post object               |
+---------------+----------+---------------------------------------------------+
| type          | string   | Type of the medium                                |
+---------------+----------+---------------------------------------------------+
