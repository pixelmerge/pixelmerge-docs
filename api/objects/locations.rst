Location Objects
================

Location objects represent locations from posts, but extracted to query them directly.

::

  {
    "id": {string},
    "username": {string},
    "service": {string},
    "time": {number},
    "position": {
      "longitude": {number},
      "latitude": {number}
    },
    "scope": {string},
    "post": {string}
  }

+---------------+--------+-----------------------------------------------------+
| Field         | Type   | Description                                         |
+===============+========+=====================================================+
| id            | string | Unique identifier of the location                   |
+---------------+--------+-----------------------------------------------------+
| username      | string | Name of the location object owner                   |
+---------------+--------+-----------------------------------------------------+
| service       | string | ID which references the service object associated   |
|               |        | with the location object                            |
+---------------+--------+-----------------------------------------------------+
| time          | number | Time of the creation of the post related to this    |
|               |        | location object                                     |
+---------------+--------+-----------------------------------------------------+
| position      | object | Object which holds the location information         |
+---------------+--------+-----------------------------------------------------+
| position[lon\ | number | Longitude of the position                           |
| gitude]       |        |                                                     |
+---------------+--------+-----------------------------------------------------+
| position[lat\ | number | Latitude of the position                            |
| itude]        |        |                                                     |
+---------------+--------+-----------------------------------------------------+
| scope         | string | Scope of the location object                        |
+---------------+--------+-----------------------------------------------------+
| post          | string | ID which references the post object, from which the |
|               |        | location data was obtained.                         |
+---------------+--------+-----------------------------------------------------+
