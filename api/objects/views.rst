View Objects
============

View objects represent the meta data used to process posts.

::

  {
    "id": {string},
    
    "settings": {object},
    "order": {number},
    "type": {string},

    "scope": {string}
  }

+----------+---------+---------------------------------------------------------+
| Field    | Type    | Description                                             |
+==========+=========+=========================================================+
| id       | string  | An unique ID which specifies a view object              |
+----------+---------+---------------------------------------------------------+
| settings | object  | Type-specific settings for the view                     |
+----------+---------+---------------------------------------------------------+
| order    | number  | Number by which the service is sorted in the user       |
|          |         | interface                                               |
+----------+---------+---------------------------------------------------------+
| type     | string  | Type of the view                                        |
+----------+---------+---------------------------------------------------------+
| scope    | string  | Scope of the view                                       |
+----------+---------+---------------------------------------------------------+
